��          \      �       �   h   �      2     ;     T     b      p     �  A  �  �   �     t  '   |     �     �  /   �  	                                           Current Main Grading Scale will be deleted. Any student grades for the current school year will be lost. Generate Grading Scale Generation Maximum Grade Minimum Grade Setup your new Grading Scale: %s Step Project-Id-Version: Grading Scale Generation plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:07+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 La Escala de Calificaciones Principal corriente estará eliminada. Las calificaciones de los estudiantes para el año escolar corriente estarán perdidos.  Generar Generación de Escala de Calificaciones Calificación Máxima Calificación Mínima Configure su nueva Escala de Calificaciones: %s Intervalo 