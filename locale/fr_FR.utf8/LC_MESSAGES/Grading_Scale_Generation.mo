��          \      �       �   h   �      2     ;     T     b      p     �    �  y   �  	      #   *     N     [  2   h  
   �                                       Current Main Grading Scale will be deleted. Any student grades for the current school year will be lost. Generate Grading Scale Generation Maximum Grade Minimum Grade Setup your new Grading Scale: %s Step Project-Id-Version: Grading Scale Generation plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:08+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 L'échelle de notation principale courante sera supprimée. Les notes des élèves pour l'école courante seront perdues. Générer Génération d'échelle de notation Note maximum Note minimum Configurez votre nouvelle échelle de notation: %s Intervalle 