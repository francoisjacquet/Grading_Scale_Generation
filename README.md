# Grading Scale Generation plugin

![screenshot](https://gitlab.com/francoisjacquet/Grading_Scale_Generation/raw/master/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Grading_Scale_Generation/

Version 11.0 - March, 2024

Author François Jacquet

Sponsored by Signo Digital, Ecuador

License GNU/GPLv2 or later

## Description

RosarioSIS plugin to generate the Grading Scale. Comes handy if your Grading Scales counts with hundreds of grades, to generate it automatically instead of creating grades one by one.

Set the minimum & maximum grades (for example: `0` to `10`), the step (for example: `0.1`, `0.5`, or `0.01`). This will generate the following grading scale: `0.0`, `0.1`, `0.2` ... `9.8`, `9.9`, `10`.

Plus an `N/A` grade (not graded, empty GPA value).

Translated in French, Spanish and Slovenian.

Note: only numeric scales can be generated.

Warning: Current Main Grading Scale will be deleted. Any student grades for the current school year will be lost. If you want to keep it, create a new Grading Scale and set its Sort Order to 1.


## Content

Plugin Configuration

- Generate

## Install

Copy the `Grading_Scale_Generation/` folder (if named `Grading_Scale_Generation-master`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Requires RosarioSIS 5+
